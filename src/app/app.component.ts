import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiServiceService } from './service/api-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'hidden-puzzle-game-two';

  showBox = true;

  topValue = 0;


  /**
   * API Reference begins
   */
  apiId = 'igh1u';

  getUrl = 'https://api.myjson.com/bins/'+this.apiId;
  postUrl = 'https://api.myjson.com/bins';
  putUrl = 'https://api.myjson.com/bins/'+this.apiId;

  apiId1 = '5ce3904074ff6726f52051d4';

  getUrl1 = 'https://api.jsonbin.io/b/'+this.apiId1;
  postUrl1 = 'https://api.jsonbin.io/b';
  putUrl1 = 'https://api.jsonbin.io/b/'+this.apiId1;

  secretKey = '$2a$10$k9oWh/xfoMdwuR.xZN3vfed2gp1VuXcZ9uU8O5md3jipILxunsiSe';//token-sneha
  headers = {
    'content-type': 'application/json',
    // 'secret-key': this.secretKey,
    'private': 'true'
  }

  data = { "score": 0 };
  /**
   * API Reference ends
   */


  constructor(private apiServiceService: ApiServiceService) {

  }

  ngOnInit() {
    //this.createJSON(this.postUrl, this.data, this.headers);
    //this.loadJSON(this.getUrl, this.headers);
    //this.data = { "score": 12 };
    //this.updateJSON(this.putUrl, this.data, this.headers);
    //this.deleteJSON(this.putUrl, this.headers);
  }

  loadJSON(url: string, headers?: any) {
    this.apiServiceService.getJSON(url, headers).subscribe((res) => {
      console.log(res);
    }, (err) => {
      console.log(err);
    });
  }

  createJSON(url: string, data: any, headers?: any) {
    this.apiServiceService.postJSON(url, data, headers).subscribe((res) => {
      console.log(res);
    }, (err) => {
      console.log(err);
    });
  }

  updateJSON(url: string, data: any, headers?: any) {
    this.apiServiceService.putJSON(url, data, headers).subscribe((res) => {
      console.log(res);
      this.loadJSON(url, this.headers);
    }, (err) => {
      console.log(err);
    });
  }

  deleteJSON(url: string, headers?: any) {
    this.apiServiceService.deleteJSON(url, headers).subscribe((res) => {
      console.log(res);
    }, (err) => {
      console.log(err);
    });
  }





}
