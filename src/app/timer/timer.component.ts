import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss']
})
export class TimerComponent implements OnInit {

  @Output('updateTimeOver') private updateTimeOver: EventEmitter<any> = new EventEmitter();
  countDownDuration = 1;//Give value in minutes
  minute = 0;
  second = 0;
  miliSecond = 0;

  strMinute = '';
  strSecond = '';
  strMiliSecond = '';
  timer;

  constructor() { }

  ngOnInit() {
    this.timer = setInterval(() => {
      //console.log(this);
      this.miliSecond++;
      if (this.miliSecond === 60) {
        this.miliSecond = 0;
        this.second++;
        //console.log(this.second);
      }
      if (this.second === 60) {
        this.second = 0;
        this.minute++;
        //console.log(this.minute);
        if (this.minute === this.countDownDuration) {
          this.strMiliSecond = this.convertToDoubleDigit(this.miliSecond);
          this.strMinute = this.convertToDoubleDigit(this.minute);
          this.strSecond = this.convertToDoubleDigit(this.second);
          this.updateTimeOver.emit({ minute: this.strMinute, second: this.strSecond, miliSecond:this.strMiliSecond})
          clearInterval(this.timer);
        }
      }
      //console.log(this.minute);
      this.strMiliSecond = this.convertToDoubleDigit(this.miliSecond);
      this.strMinute = this.convertToDoubleDigit(this.minute);
      this.strSecond = this.convertToDoubleDigit(this.second);
    }, 1000 / 60);
  }

  convertToDoubleDigit(value) {
    if (String(value).length === 1) {
      return '0' + String(value);
    } else {
      return value;
    }
  }

}
