import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class GlobalDataService {
  data = {
    screen_1:{
      game_started:false
    },
    screen_2:{
      totalScore:0,
      timeLimit:1000
    },
    screen_3:{

    }
  }

  constructor() { }
}
